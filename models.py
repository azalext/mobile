# -*- coding: utf-8 -*-

from azalinc.shortcuts import *

from azalext.system.models import *

#*********************************************************************

BACKEND_TYPEs = (
    ('cache', "Memcache daemon"),
    ('redis', "Redis or Sentinel"),

    ('sqldb', "MySQL or PostgreSQL"),
    ('nosql', "MongoDB database"),
    ('neo4j', "Neo4j instance"),

    ('parse', "Parse Server"),
    ('graph', "GraphQL engine"),

    ('queue', "AMQP instance"),
    ('topic', "MQTT instance"),
)

################################################################################

class Machine(models.Model):
    user    = models.ForeignKey(Person, related_name='machines')
    name    = models.CharField(max_length=64)

    kind    = models.ForeignKey(HardwareProfile, related_name='machines')
    runs    = models.ForeignKey(OperatingSystem, related_name='machines', blank=True, null=True, default=None)

    hw_addr  = models.CharField(max_length=256, blank=True)
    ip4addr  = IPAddressField(protocol='ipv4')
    ip6addr  = IPAddressField(protocol='ipv6')

    username = models.CharField(max_length=256, default='root')
    password = models.CharField(max_length=256, blank=True)

    def __str__(self): return str(self.alias)

#*******************************************************************************

class Device(models.Model):
    parent    = models.ForeignKey(Machine, related_name='devices')
    narrow    = models.CharField(max_length=64)
    driver    = models.ForeignKey(HardwareAccessory, related_name='devices')

    hw_addr  = models.CharField(max_length=256, blank=True)
    ip4addr  = IPAddressField(protocol='ipv4')
    ip6addr  = IPAddressField(protocol='ipv6')

    username = models.CharField(max_length=256, default='root')
    password = models.CharField(max_length=256, blank=True)

    def __str__(self): return str(self.alias)

################################################################################


