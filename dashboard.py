from controlcenter import Dashboard, widgets

from azalext.mobile.models import *

################################################################################

class MachineList(widgets.ItemList):
    model = Machine
    list_display = ('name','user','kind','runs')

#*******************************************************************************

class DeviceList(widgets.ItemList):
    model = Device
    list_display = ('parent','narrow','driver')

################################################################################

class MySingleBarChart(widgets.SingleBarChart):
    # label and series
    values_list = ('username', 'score')
    # Data source
    queryset = Person.objects.order_by('-score')
    limit_to = 3

################################################################################

class Landing(Dashboard):
    title = 'Mobile'

    widgets = (
        MachineList,
        DeviceList,

        #MySingleBarChart,
    )

