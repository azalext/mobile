from django.conf.urls import include, url

from . import views

urlpatterns = [
    url(r'^$',                             views.homepage),

    url(r'^(?P<uid>.+)/$',                 views.site_landing),
    url(r'^(?P<uid>.+)/(?P<pid>[0-9]+)$',  views.site_product),

    url(r'^(?P<uid>.+)/refresh/?$',        views.site_refresh),
    url(r'^(?P<uid>.+)/dataset$',          views.site_dataset),
]
