# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import *

####################################################################

class MachineAdmin(admin.ModelAdmin):
    list_display  = ['name','user','kind','hw_addr','ip4addr','ip6addr']
    list_filter   = ['user__username','runs__alias','kind__alias']
    #list_editable = ['name','fqdn']

    def tools(self, record):
        resp = {}

        resp['View'] = "/shops/%(id)s/"        % record.__dict__
        resp['Sync'] = "/shops/%(id)s/refresh" % record.__dict__

        return '&nbsp;'.join(['<a href="%s">%s</a>' % (v,k) for k,v in resp.iteritems()])
    tools.allow_tags = True

    def count(self, record):
        return len(record.datasets.all())

admin.site.register(Machine, MachineAdmin)

####################################################################

class DeviceAdmin(admin.ModelAdmin):
    list_display  = ['parent','narrow','driver','hw_addr','ip4addr','ip6addr']
    #list_filter   = ['user__username']
    #list_editable = ['name','fqdn']

admin.site.register(Device, DeviceAdmin)

